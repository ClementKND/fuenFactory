'use strict';

// ██╗    ██╗███████╗██╗      ██████╗ ██████╗ ███╗   ███╗███████╗                                
// ██║    ██║██╔════╝██║     ██╔════╝██╔═══██╗████╗ ████║██╔════╝                                
// ██║ █╗ ██║█████╗  ██║     ██║     ██║   ██║██╔████╔██║█████╗                                  
// ██║███╗██║██╔══╝  ██║     ██║     ██║   ██║██║╚██╔╝██║██╔══╝                                  
// ╚███╔███╔╝███████╗███████╗╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗                                
//  ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝                                
                                                                                                 
// ████████╗ ██████╗                                                                             
// ╚══██╔══╝██╔═══██╗                                                                            
//    ██║   ██║   ██║                                                                            
//    ██║   ██║   ██║                                                                            
//    ██║   ╚██████╔╝                                                                            
//    ╚═╝    ╚═════╝                                                                             
                                                                                                 
// ███████╗██╗   ██╗███████╗███╗   ██╗███████╗ █████╗  ██████╗████████╗ ██████╗ ██████╗ ██╗   ██╗
// ██╔════╝██║   ██║██╔════╝████╗  ██║██╔════╝██╔══██╗██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝
// █████╗  ██║   ██║█████╗  ██╔██╗ ██║█████╗  ███████║██║        ██║   ██║   ██║██████╔╝ ╚████╔╝ 
// ██╔══╝  ██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██║██║        ██║   ██║   ██║██╔══██╗  ╚██╔╝  
// ██║     ╚██████╔╝███████╗██║ ╚████║██║     ██║  ██║╚██████╗   ██║   ╚██████╔╝██║  ██║   ██║   
// ╚═╝      ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝   

const config        = require( './gulp.config' );
const beep          = require( 'beepbeep' );
const browserSync   = require( 'browser-sync' ).create();
// const fs            = require( 'fs' );
const gulp          = require( 'gulp' );
const autoprefixer  = require( 'gulp-autoprefixer' );
const avif          = require( 'gulp-avif' );
const babel         = require( 'gulp-babel' );
const changeFile    = require( 'gulp-change-file-content' );
const clean         = require( 'gulp-clean' );
// const minifyCss     = require( 'gulp-clean-css' );
const concat        = require( 'gulp-concat' );
const eslint        = require( 'gulp-eslint' );
const fontmin       = require( 'gulp-fontmin' );
const image         = require( 'gulp-image' );
const imgResize     = require( 'gulp-image-resize' );
const jsonMinify    = require( 'gulp-json-minify' );
const minify        = require( 'gulp-minify' );
const notify        = require( 'gulp-notify' );
const plumber       = require( 'gulp-plumber' );
const rename        = require( 'gulp-rename' );
// const sourcemaps    = require( 'gulp-sourcemaps' );
const stylus        = require( 'gulp-stylus' );
const styleLint     = require( 'gulp-stylelint' );
const ttf2woff2     = require( 'gulp-ttf2woff2' );
const uglify        = require( 'gulp-uglify' );
// const uglifyCss     = require( 'gulp-uglifycss' );
const webp          = require( 'gulp-webp' );


// >> Notify ERROR
const errorHandler = ( err ) => {
    notify.onError({
        title:          "❌ - ERROR !",
        message:        "<%= error.message %> " + err,
        icon:           config.notifIcon
    })
    beep();
}

// >> Create Browser Sync Task 
gulp.task( 'sync', () => {
    browserSync.init({
        proxy:              config.projectURL,
        ghostMode:          config.ghostMode,
        open:               config.browserAutoOpen,
        injectChanges:      config.injectChanges,
        watchEvents:        config.watchEvents,
        notify:             config.notify
    })
})


// >> Watch files
gulp.task( 'watch', () => {
    gulp.watch( config.stylusDir, gulp.series( 'stylus' ) )
    gulp.watch( config.scriptDir, gulp.series( 'script' ) )
    gulp.watch( "./scripts/json/*.json" ).on( "change", browserSync.reload )
    gulp.watch( "./*.php" ).on( "change", browserSync.reload )
    gulp.watch( "./templates/*.tpl" ).on( "change", browserSync.reload )
    gulp.watch( "./templates/**/*.tpl" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/js/**/*.js" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/css/*.css" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/json/*.json" ).on( "change", browserSync.reload )
} ) 


// >>> PICTURES PROCESS <<< \\

// Resize pictures
gulp.task( 'image-resize', ( done ) => {
    config.imagesQuery.forEach( ( size ) => {
        return gulp.src( config.imageDirFiles )
            .pipe( imgResize({
                width: size,
                height: size,
                upscale: false
            }) )
            .pipe( rename( ( path ) => {
                path.basename = `${path.basename}@${size}w`;
            } ))
            .pipe( gulp.dest( config.imageDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            }) )
    })
    done();
} )

// >> COMPRESS PICTURES
// > PNG & JPG COMPRESS

// PNG COMPRESS
gulp.task( 'png-compress', () => {
    return gulp.src( config.pngDirFiles )
        .pipe( image( config.imageOptions ) )
        .pipe( gulp.dest( config.pngCompressed ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
})

// JPG COMPRESS
gulp.task( 'jpg-compress', () => {
    return gulp.src( config.jpgDirFiles )
        .pipe( image( config.imageOptions ) )
        .pipe( gulp.dest( config.jpgCompressed ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
})

// >>> RESIZE IMAGES
// >> COMPRESSED IMAGES RESIZE
// PNG
gulp.task( 'png-resize', ( done ) => {
    config.imagesQuery.forEach( ( size ) => {
        return gulp.src( config.pngCompressedFiles )
            .pipe( imgResize({
                width:      size,
                height:     size,
                upscale:    false
            }) )
            .pipe( rename( ( path ) => {
                path.basename = `${path.basename}@${size}w`;
            } ) )
            .pipe( gulp.dest( config.pngResized ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    } )
    return done();
} )

// JPG
gulp.task( 'jpg-resize', ( done ) => {
    config.imagesQuery.forEach( ( size ) => {
        return gulp.src( config.jpgCompressedFiles )
            .pipe( imgResize({
                width:      size,
                height:     size,
                upscale:    false
            }) )
            .pipe( rename( ( path ) => {
                path.basename = `${path.basename}@${size}w`;
            } ) )
            .pipe( gulp.dest( config.jpgResized  ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    } )
    return done();
} )

// >>> AVIF CONVERT
// PNG
gulp.task( 'png-avif', () => {
    setTimeout( () => {
        return gulp.src( config.pngResizedFiles )
            .pipe( avif() )
            .pipe( gulp.dest( config.avifDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    }, 3000 )
} )

// JPG
gulp.task( 'jpg-avif', () => {
    setTimeout( () => {
        return gulp.src( config.jpgResizedFiles )
            .pipe( avif() )
            .pipe( gulp.dest( config.avifDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    }, 3000 )
} )

// >>> WEBP COMVERT
// PNG
gulp.task( 'png-webp', ( done ) => {
    setTimeout( () => {
        return gulp.src( config.pngResizedFiles )
        .pipe( webp() )
        .pipe( gulp.dest( config.webpDest ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
    }, 3000 )
    return done();
} )

// JPG
gulp.task( 'jpg-webp', ( done ) => {
    setTimeout( () => {
        return gulp.src( config.jpgCompressedFiles )
            .pipe( webp() )
            .pipe( gulp.dest( config.webpDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    }, 3000 )
    return done();
} )

// >> MOVE PICTURES
// > PNG & JPG MOVER

// PNG MOVE
gulp.task( 'png-move', () => {
    return gulp.src( config.pngCompressedFiles )
        .pipe( gulp.dest( config.assetsPngDir ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
})

// JPG MOVE
gulp.task( 'jpg-move', () => {
    return gulp.src( config.jpgCompressedFiles )
        .pipe( gulp.dest( config.assetsJpgDir ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
    })    

// >>> SCRIPTS PROCESS <<< \\

// > Eslint config
gulp.task( 'eslint', () => {
    return gulp.src( './scripts/**/*.js' )
        .pipe( eslint({
            "configFile": ".eslintrc"
        }) )
        .pipe( eslint.format() )
        .pipe( eslint.failOnError() );
} )

// > Minify JS Files
gulp.task( 'minify', () => {
    return gulp.src( './scripts/**/*.js' )
        .pipe( minify({
            ext: {
                min:        '.js'
            },
            noSource:       true
        }) )
        // .pipe( gulp.dest( './scripts/_minify/' ) )
        .pipe( uglify() )
        .pipe( concat( 'custom.js' ) )
        .pipe( changeFile( ( content ) => {
            const start = 'window.onload=()=>{\n';
            const end = '\n}';
            return `${ start }${ content }${ end }`;
        } ) )
        .pipe( gulp.dest( './assets/js/' ) )
} )

// > Minify JSON Files
gulp.task( 'minify-json', () => {
    return gulp.src( './scripts/json/*.json' )
        .pipe( jsonMinify() )
        .pipe( gulp.dest( './assets/json/' ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
} )

// > Babelise, Uglify and concat JS Files
gulp.task( 'uglify', () => {
    return gulp.src( './scripts/_minify/*.js' )
        .pipe( babel({
            presets:        [ "@babel/preset-env" ]
        }) )
        // .pipe( uglify() )
        // .pipe( concat( 'script.js' ) )
        // .pipe( gulp.dest( '../assets/js/' ) )
} )


// >>> STYLE PROCESS <<< \\

// Compile Stylus
gulp.task( 'stylus', () => {
    return gulp.src( config.stylusIndex )
        .pipe( plumber( errorHandler ) )
        .pipe( styleLint( config.stylusLinter ) )
        .pipe( stylus( { 
            compress: false,
            url: 'embedurl'    
        } ) )
        .pipe( autoprefixer( { cascade: false } ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
        // .pipe( uglifyCss( config.uglifyOptions ) ) 
        // .pipe( minifyCss() )
        .pipe( gulp.dest( config.cssDir ) )
        .pipe( notify({
            title:          "Stylus => CSS",
            message:        "Travail compilé !",
            icon:           config.notifIcon
        }) )
        .pipe( browserSync.reload({
            stream:         true
        }) )
})

// >>> FONTS PROCESS

// > Font min ( CSS / EOT / SVG / TTF / WOFF)
gulp.task( 'font-min', () => {
    return gulp.src( config.fontsDir )
        .pipe( fontmin() )
        .pipe( gulp.dest( ( path ) => {
            if( path.extname === '.css' ) {
                return config.assetsDest; 
            }
            if( path.extname === '.eot' ) {
                return config.assetsDest; 
            }
            if( path.extname === '.svg' ) {
                return config.assetsDest; 
            } 
            if( path.extname === '.ttf' ) {
                return config.assetsDest; 
            } 
            if( path.extname === '.woff' ) {
                return config.assetsDest; 
            }
            if( path.extname === '.woff2' ) {
                return config.assetsDest;
            } else {
                return config.assetsDest;
            }
        } ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
} )

gulp.task( 'font-woff2', () => {
    return gulp.src( config.fontsDir )
        .pipe( ttf2woff2() )
        .pipe( gulp.dest( config.assetsDest ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
} )

// >>> SVG PROCESS

// > SVG Compress
gulp.task( 'svg-compress', () => {
    return gulp.src( config.svgDirFiles )
        .pipe( image( config.imageOptions ) )
        .pipe( gulp.dest( config.svgDest ))
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        }) )
} )

// > SVG to B64
// ### TODO



// >>> CLEAN SOCLE
gulp.task( 'clean', async () => {
    return gulp.src( config.cleanDir, { allowEmpty: true } )
        .pipe( clean( { force: true } ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
} )

// >>> PARALLELS/SERIES GULP TASK <<< \\

// > Gulp parallel compress images
gulp.task( 'image-compress', gulp.parallel( 'jpg-compress', 'png-compress' ) );

// > Gulp parallel resize images
gulp.task( 'image-resize', gulp.parallel( 'jpg-resize', 'png-resize' ) );

// > Gulp parallel avif images
gulp.task( 'image-avif', gulp.parallel( 'jpg-avif', 'png-avif' ) );

// > Gulp parallel webp images
gulp.task( 'image-webp', gulp.parallel( 'jpg-webp', 'png-webp' ) );

// > Gulp parallel images mover
gulp.task( 'image-mover', gulp.parallel( 'jpg-move', 'png-move' ) );

// >> Gulp series process images
// gulp.task( 'image', gulp.series( 'image-compress', 'image-resize', 'image-avif', 'image-webp' ) );
gulp.task( 'image', gulp.series( 'image-compress', 'image-webp', 'image-mover' ) );

// > Gulp series fonts
gulp.task( 'font', gulp.series( 'font-min', 'font-woff2' ) );

// > Gulp series scripts
gulp.task( 'script', gulp.series( 'minify', 'uglify', 'minify-json' ) );

// >> Parallel gulp default launch
gulp.task( 'default', gulp.parallel( 'watch', 'sync' ) );
