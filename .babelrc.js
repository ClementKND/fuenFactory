const presets = [
    ["@babel/env", {
        "useBuiltIns":          true,
        "debug":                false,
        "configPath":           "./scripts/",
        "corejs":               3
    }]
];

const plugins = [
    ["@babel/plugin-transform-runtime", {
        "corejs":       false,
        "helpers":      true,
        "regenerator":  true,
        "useESModules": false
    }],
    "@babel/plugin-transform-spread",
    "@babel/plugin-proposal-object-rest-spread",
    "@babel/plugin-transform-template-literals",
    "@babel/plugin-transform-arrow-functions",
    "@babel/plugin-proposal-class-properties"
];

module.exports = {
    presets,
    plugins
};