let gulp, image, notify, config;

gulp = require('gulp');
image = require('gulp-image');
notify = require('gulp-notify');

config = require('./config');

gulp.task( 'image-compress', () => {
    return gulp.src( config.pngDirFiles ) // Need to change to add differents extensions
        .pipe( image( config.imageOptions ) )
        .pipe( gulp.dest( config.pngCompressed ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
})