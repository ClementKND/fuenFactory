let gulp, webp, notify, config;

gulp = require('gulp');
webp = require('gulp-webp');
notify = require('gulp-notify');

config = require('./config');

gulp.task( 'png-webp', ( done ) => {
    setTimeout( () => {
        return gulp.src( config.pngResizedFiles ) // Need to change to add differents extensions
        .pipe( webp() )
        .pipe( gulp.dest( config.webpDest ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
    }, 3000 )
    return done();
} )