let gulp, jsonMinify, notify;

gulp            = require('gulp');
jsonMinify      = require('gulp-json-minify');
notify          = require('gulp-notify');

gulp.task( 'minify-json', () => {
    return gulp.src( './scripts/json/*.json' )
        .pipe( jsonMinify() )
        .pipe( gulp.dest( './assets/json/' ) )
        .on( 'error', notify.onError( ( error ) => {
            return error.message;
        } ) )
} )

// Need to export variables from config file