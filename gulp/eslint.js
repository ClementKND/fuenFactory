let gulp, eslint;

gulp = require('gulp');
eslint = require('gulp-eslint');

gulp.task( 'eslint', () => {
    return gulp.src( './scripts/**/*.js' )
        .pipe( eslint({
            "configFile": "../.eslintrc"
        }) )
        .pipe( eslint.format() )
        .pipe( eslint.failOnError() );
} )