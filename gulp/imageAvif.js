let gulp, avif, notify, config;

gulp = require('gulp');
avif = require('gulp-avif');
notify = require('gulp-notify');

config = require('config');

gulp.task( 'image-avif', () => {
    setTimeout( () => {
        return gulp.src( config.pngResizedFiles ) // Need to change to add differents extensions
            .pipe( avif() )
            .pipe( gulp.dest( config.avifDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            } ) )
    }, 3000 )
} )