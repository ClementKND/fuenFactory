let gulp, browserSync, config;

gulp            = require('gulp');
browserSync     = require('browser-sync');

config          = require('./config');

gulp.task( 'watch', () => {
    gulp.watch( config.stylusDir, gulp.series( 'stylus' ) )
    gulp.watch( config.scriptDir, gulp.series( 'script' ) )
    gulp.watch( "./scripts/json/*.json" ).on( "change", browserSync.reload )
    gulp.watch( "./*.php" ).on( "change", browserSync.reload )
    gulp.watch( "./templates/*.tpl" ).on( "change", browserSync.reload )
    gulp.watch( "./templates/**/*.tpl" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/js/**/*.js" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/css/*.css" ).on( "change", browserSync.reload )
    gulp.watch( "./assets/json/*.json" ).on( "change", browserSync.reload )
} ) 
