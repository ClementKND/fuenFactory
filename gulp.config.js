/**
 * WPGulp Configuration File
 *
 * 1. Edit the variables as per your project requirements.
 * 2. In paths you can add <<glob or array of globs>>.
 *
 * @package WPGulp
 */

// >>> PROJECTS OPTIONS

// >>> Local project URL of your already running WordPress site.
// >> Could be something like "wpgulp.local" or "localhost"
// > Depending upon your local WordPress setup.

const projectURL                    = 'localhost:8888/brec';

const $MAIN_DIR                     = "."

// $ASSETS = EXPORT FOLDER
const $ASSETS                       = `${$MAIN_DIR}/assets`;
const $IMAGE                        = `${$MAIN_DIR}/images`;
const $SCRIPTS                      = `${$MAIN_DIR}/scripts`;
const $FONTS                        = `${$MAIN_DIR}/fonts`;

// > Theme/Plugin URL. Leave it like it is; since our gulpfile.js lives in the root folder.
const productURL                    = './';
const browserAutoOpen               = true;
const injectChanges                 = true;
const ghostMode                     = false;
const watchEvents                   = [ 'change', 'add', 'unlink', 'addDir', 'unlinkDir'];
const notify                        = true;

// > Notify Icon
const notifIcon     = "https://media.moddb.com/images/mods/1/28/27592/.5.jpg"

// >>> STYLE OPTIONS.
// Path to main .styl file.
const stylusIndex                   = `${$MAIN_DIR}/stylus/style.styl`;

// Path to all .styl files.
const stylusDir                     = `${$MAIN_DIR}/stylus/**/*.styl`;

const scriptDir                     = `${$MAIN_DIR}/scripts/**/*.js`;

// Path to compiled CSS.
const cssDir                        = `${$ASSETS}/css/`;
const cssSheets                     = `${$ASSETS}/css/style.css`;
const cssDel                        = `${$ASSETS}/css/*.css`;

// >>> Images options.
// >> Options Images Property
// > Input images path
const imageDirFiles                 = `${$IMAGE}/**/*.{png, jpg}`;
const pngDirFiles                   = `${$IMAGE}/png/*.png`;
const jpgDirFiles                   = `${$IMAGE}/jpg/*.jpg`;
const pngCompressedFiles            = `${$IMAGE}/_compressed/png/*.png`;
const jpgCompressedFiles            = `${$IMAGE}/_compressed/jpg/*.jpg`;
const pngResizedFiles               = `${$IMAGE}/_resized/png/*.png`;
const jpgResizedFiles               = `${$IMAGE}/_resized/jpg/*.jpg`;
const svgDirFiles                   = `${$IMAGE}/svg/*.svg`;

// > Output images path
const imageDest                     = `${$ASSETS}/img/`;
const pngCompressed                 = `${$IMAGE}/_compressed/png/`;
const jpgCompressed                 = `${$IMAGE}/_compressed/jpg/`;
const pngResized                    = `${$IMAGE}/_resized/png/`
const jpgResized                    = `${$IMAGE}/_resized/jpg/`
const avifDest                      = `${$ASSETS}/img/avif/`;
const webpDest                      = `${$ASSETS}/img/webp/`;
const svgDest                       = `${$ASSETS}/img/svg/`;

// >>> IMAGES OPTIONS.
// > Compress options
const imageOptions = {
    optipng: ['-i 1', '-strip all', '-fix', '-o7', '-force'],
    pngquant: ['--speed=1', '--force', 256],
    zopflipng: ['-y', '--lossy_8bit', '--lossy_transparent'],
    jpegRecompress: ['--strip', '--quality', 'medium', '--min', 40, '--max', 80],
    mozjpeg: ['-optimize', '-progressive'],
    gifsicle: ['--optimize'],
    svgo: ['--enable', 'cleanupIDs', '--disable', 'convertColors']
}

// > Resize options.
const imagesQuery = [ 1920, 1680, 1366, 1024, 768, 375 ];

// >> CSS Options
// Options CSS Property
const stylusLinter = { fix: true, failAfterError: true, reporters: 
    [{
        formatter: 'verbose', 
        console: true
    }], debug: true }
const uglifyOptions = { "maxLineLen": 80, "uglyComments": true }

// >> Clean Options
// > Dir Path
const compressedDir                 = `${$IMAGE}/_compressed/`;
const resizedDir                    = `${$IMAGE}/_resized/`;
const minifyDir                     = `${$SCRIPTS}/_minify/`;
const assetsAvifDir                 = `${$ASSETS}/img/avif/`;
const assetsSvgDir                  = `${$ASSETS}/img/svg`;
const assetsWebpDir                 = `${$ASSETS}/img/webp/`;
const assetsJpgDir                  = `${$ASSETS}/img/jpg/`;
const assetsPngDir                  = `${$ASSETS}/img/png/`;
const assetsJsDir                   = `${$ASSETS}/js/*`


// >>> FONTS
// > Fonts dir
const fontsDir                      = `${$FONTS}/*.ttf`;
const assetsDest                    = `${$ASSETS}/fonts/`;
const cssFontDest                   = `${$ASSETS}/fonts/css/`;
const eotDest                       = `${$ASSETS}/fonts/eot/`;
const svgFontDest                   = `${$ASSETS}/fonts/svg/`;
const ttfDest                       = `${$ASSETS}/fonts/ttf/`;
const woffDest                      = `${$ASSETS}/fonts/woff/`;
const woff2Dest                     = `${$ASSETS}/fonts/woff2/`;

// > Array of dir
const cleanDir = [ 
    compressedDir, 
    resizedDir, 
    minifyDir,
    assetsAvifDir, 
    assetsSvgDir,
    assetsWebpDir, 
    assetsJpgDir,
    assetsPngDir,
    assetsJsDir,
    cssFontDest,
    eotDest,
    svgFontDest,
    ttfDest, 
    woffDest,
    woff2Dest 
];

// >>> Export modules
// Please export in the good order for easy debug
module.exports = {
    projectURL,
    productURL,
    browserAutoOpen,
    injectChanges,
    ghostMode,
    watchEvents,
    notify,
    notifIcon,
    stylusIndex,
    stylusDir,
    scriptDir,
    cssDir,
    cssSheets,
    cssDel,
    imageDirFiles,
    pngDirFiles,
    jpgDirFiles,
    pngCompressedFiles,
    jpgCompressedFiles,
    pngResizedFiles,
    jpgResizedFiles,
    svgDirFiles,
    imageDest,
    pngCompressed,
    jpgCompressed,
    pngResized,
    jpgResized,
    avifDest,
    webpDest,
    svgDest,
    imageOptions,
    imagesQuery,
    stylusLinter,
    uglifyOptions,
    assetsAvifDir,
    assetsWebpDir,
    assetsSvgDir,
    assetsJpgDir,
    assetsPngDir,
    cleanDir,
    fontsDir,
    assetsDest,
    cssFontDest,
    eotDest,
    svgFontDest,
    ttfDest,
    woffDest,
    woff2Dest
}
